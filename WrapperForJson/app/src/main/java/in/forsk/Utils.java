package in.forsk;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

import in.forsk.wrapper.FacultyWrapper;

/**
 * Created by Saurabh on 3/28/2016.
 */
public class Utils {

    private final static String TAG = Utils.class.getSimpleName();

    /**
     * Method to convert the Input stream to string.
     *
     * @param is
     * @return
     * @throws IOException
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }


    /**
     * Method to read the json file from RAW folder.
     *
     * @param context
     * @param resourceId
     * @return
     * @throws IOException
     */
    public static String getStringFromRaw(Context context, int resourceId)
            throws IOException {
        // Reading File from resource folder
        Resources r = context.getResources();
        InputStream is = r.openRawResource(resourceId);
        String statesText = convertStreamToString(is);
        is.close();

        Log.d(TAG, statesText);

        return statesText;
    }

    public static ArrayList<FacultyWrapper> pasreLocalFacultyJson(String json_string) {


        ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
        try {
            //Converting multiple json data (String) into Json array
            //Every faculty is represented as a json object in the file.
            //so we need to store them in json array.
            JSONArray facultyArray = new JSONArray(json_string);
            Log.d(TAG, facultyArray.toString());
            // Iterating json array into json objects
            for (int i = 0; facultyArray.length() > i; i++) {

                // Extracting json object from particular index of array
                JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

                // Design patterns
                FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

                printObject(facultyObject);

                mFacultyDataList.add(facultyObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mFacultyDataList;
    }

    public static void printObject(FacultyWrapper obj) {
        // Operator Overloading
        Log.d(TAG, "First Name : " + obj.getFirst_name());
        Log.d(TAG, "Last Name : " + obj.getLast_name());
        Log.d(TAG, "Photo : " + obj.getPhoto());
        Log.d(TAG, "Department : " + obj.getDepartment());
        Log.d(TAG, "reserch_area : " + obj.getReserch_area());

        for (String s : obj.getInterest_areas()) {
            Log.d(TAG, "Interest Area : " + s);
        }
        for (String s : obj.getContact_details()) {
            Log.d(TAG, "Contact Detail : " + s);
        }
    }
}
